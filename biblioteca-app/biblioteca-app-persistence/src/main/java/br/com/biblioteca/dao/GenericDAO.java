package br.com.biblioteca.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class GenericDAO<T> {
	
	//private EntityManager entity = JPAUtil.getCreateEntityManager();
	
	@Inject
	private EntityManager entity;
	
	public void salvar(T obj){
		try{
			entity.getTransaction().begin();
			entity.persist(obj);
		}catch(Exception e){			
			entity.getTransaction().rollback();
			entity.close();
		}finally {
			entity.getTransaction().commit();
			entity.close();
		}
	}
	public void alterar(T obj){
		try{
			entity.getTransaction().begin();
			entity.merge(obj);
		}catch(Exception e){			
			entity.getTransaction().rollback();
			entity.close();
		}finally {
			entity.getTransaction().commit();
			entity.close();
		}
	}
	public void excluir(T obj){
		try{
			entity.getTransaction().begin();
			entity.remove(obj);
		}catch(Exception e){			
			entity.getTransaction().rollback();
			entity.close();
		}finally {
			entity.getTransaction().commit();
			entity.close();
		}
	}
	/**
	 * @return the entity
	 */
	public EntityManager getEntity() {
		return entity;
	}
	
	

}
