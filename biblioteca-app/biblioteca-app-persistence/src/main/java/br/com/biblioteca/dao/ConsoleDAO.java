package br.com.biblioteca.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.biblioteca.entity.Console;

public class ConsoleDAO extends GenericDAO<Console>{
	
	public ConsoleDAO() {
		super();
	}

	public Console buscarConsolePorId(Integer id) {		
		return getEntity().find(Console.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Console> buscarTodosOsConsoles() {
		StringBuilder SqlBuilder = new StringBuilder();
		SqlBuilder.append("Select c from Console c ");
		SqlBuilder.append("left join fetch c.empresa ");
		SqlBuilder.append("left join fetch c.jogos ");

		Query query = getEntity().createQuery(SqlBuilder.toString());		
		return query.getResultList();
	}

}
