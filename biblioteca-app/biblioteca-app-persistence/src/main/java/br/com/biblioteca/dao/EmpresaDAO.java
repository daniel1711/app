package br.com.biblioteca.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.biblioteca.entity.Empresa;

public class EmpresaDAO extends GenericDAO<Empresa>{

	public Empresa buscarEmpresaPorId(Integer id) {
		return getEntity().find(Empresa.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Empresa> buscarTodosOsEmpresas() {
		StringBuilder SqlBuilder = new StringBuilder();
		SqlBuilder.append("Select e from Empresa e ");
		Query query = getEntity().createQuery(SqlBuilder.toString());		
		return query.getResultList();
	}

}
