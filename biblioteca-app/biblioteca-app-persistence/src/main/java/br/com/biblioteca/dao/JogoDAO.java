package br.com.biblioteca.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.biblioteca.entity.Jogo;

public class JogoDAO extends GenericDAO<Jogo>{

	public Jogo buscarJogoPorId(Integer id) {
		return getEntity().find(Jogo.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Jogo> buscarTodosOsJogos() {
		StringBuilder SqlBuilder = new StringBuilder();
		SqlBuilder.append("Select j from Jogo j ");
		Query query = getEntity().createQuery(SqlBuilder.toString());		
		return query.getResultList();
	}

}
