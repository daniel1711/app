angular.module('myapp', []).controller('gameCTRL', function($scope, $http){
	
	$http.get("http://localhost:8082/biblioteca-app-web/api/console")
    .then(function(response) {
        $scope.consoles = response.data;
    });
	
	$http.get("http://localhost:8082/biblioteca-app-web/api/empresa")
    .then(function(response) {
        $scope.empresas = response.data;
    });
	
	$scope.salvar = function(console) {
		$http.post("http://localhost:8082/biblioteca-app-web/api/console", console)
	    .then(function(response) {
	      
	    });
	}
	
	$scope.alterar = function(console) {
		$http.put("http://localhost:8082/biblioteca-app-web/api/console", console)
	    .then(function(response) {
	      
	    });
	}
	
	$scope.excluir = function(id) {
		$http.delete("http://localhost:8082/biblioteca-app-web/api/console/" +id)
	    .then(function(response) {
	      
	    });
	}
	
	$scope.selecionar = function(console) {
		$scope.console = console;
	}
	
	
})