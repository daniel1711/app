angular.module('myapp', []).controller('jogoCTRL', function($scope, $http){
	
	$http.get("http://localhost:8082/biblioteca-app-web/api/jogo")
    .then(function(response) {
        $scope.jogos = response.data;
    });
	
	$http.get("http://localhost:8082/biblioteca-app-web/api/console")
    .then(function(response) {
        $scope.consoles = response.data;
    });
	
	$scope.salvar = function(jogo) {
		$http.post("http://localhost:8082/biblioteca-app-web/api/jogo", jogo)
	    .then(function(response) {
	      
	    });
	}
	
	$scope.alterar = function(jogo) {
		$http.put("http://localhost:8082/biblioteca-app-web/api/jogo", jogo)
	    .then(function(response) {
	      
	    });
	}
	
	$scope.excluir = function(id) {
		$http.delete("http://localhost:8082/biblioteca-app-web/api/jogo/" +id)
	    .then(function(response) {
	      
	    });
	}
	
	$scope.selecionar = function(jogo) {
		$scope.jogo = jogo;
	}
	
	
})