var app = angular.module("myapp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/inicio.html"
    })
    .when("/listJogo", {
    	templateUrl : "views/jogo/listaJogo.html",
    	controller : "listaJogoCTRL"
    })
    .when("/formJogo", {
    	templateUrl : "views/jogo/listaJogo.html",
    	controller : "listaJogoCTRL"
    })
    .when("/listEmpresa", {
        templateUrl : "views/empresa/listaEmpresa.html",
        controller : "listaEmpresaCTRL"
    })
    .when("/formEmpresa", {
        templateUrl : "views/empresa/formEmpresa.html",
        controller : "listaEmpresaCTRL"
    })
    .when("/listConsole", {
        templateUrl : "views/console/listaConsole.html",
        controller : "listaConsoleCTRL"
    });
});