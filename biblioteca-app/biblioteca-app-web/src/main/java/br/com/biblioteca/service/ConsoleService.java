package br.com.biblioteca.service;



import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.biblioteca.dao.ConsoleDAO;
import br.com.biblioteca.entity.Console;
import br.com.biblioteca.exception.BibliotecaException;
import io.swagger.annotations.ApiParam;

@Path("console")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class ConsoleService {
	
	@Inject
	private ConsoleDAO dao;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvar(@ApiParam(value="console", required=true) final Console console){
		if(console == null) 
			throw new BibliotecaException("Dados n�o informados");
		dao.salvar(console);		
		return Response.ok(console).build();
	}
	
	@PUT
	public Response atualizar(@ApiParam(value="autor", required=true) final Console console){
		if(console == null) 
			throw new BibliotecaException("Dados n�o informados");
		dao.alterar(console);		
		return Response.ok(console).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response excluir(@PathParam("id") final Integer id){
		if(id == null) 
			throw new BibliotecaException("Dados n�o informados");		
		Console console = dao.buscarConsolePorId(id);		
		dao.excluir(console);		
		return Response.ok("Excluido com sucesso!").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response obterTodosOsAutores(){
		List<Console> consoles = dao.buscarTodosOsConsoles();		
		return Response.ok(consoles).build();
	}

}
