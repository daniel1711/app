package br.com.biblioteca.service;



import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.biblioteca.dao.EmpresaDAO;
import br.com.biblioteca.entity.Empresa;
import br.com.biblioteca.exception.BibliotecaException;
import io.swagger.annotations.ApiParam;

@Path("empresa")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class EmpresaService {
	
	@Inject
	private EmpresaDAO dao;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvar(@ApiParam(value="empresa", required=true) final Empresa empresa){
		if(empresa == null) 
			throw new BibliotecaException("Dados n�o informados");
		dao.salvar(empresa);		
		return Response.ok(empresa).build();
	}
	
	@PUT
	public Response atualizar(@ApiParam(value="autor", required=true) final Empresa empresa){
		if(empresa == null) 
			throw new BibliotecaException("Dados n�o informados");
		dao.alterar(empresa);		
		return Response.ok(empresa).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response excluir(@PathParam("id") final Integer id){
		if(id == null) 
			throw new BibliotecaException("Dados n�o informados");		
		Empresa empresa = dao.buscarEmpresaPorId(id);		
		dao.excluir(empresa);		
		return Response.ok("Excluido com sucesso!").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response obterTodosOsAutores(){
		List<Empresa> empresas = dao.buscarTodosOsEmpresas();		
		return Response.ok(empresas).build();
	}

}
