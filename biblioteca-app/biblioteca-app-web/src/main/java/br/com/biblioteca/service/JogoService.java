package br.com.biblioteca.service;



import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.biblioteca.dao.JogoDAO;
import br.com.biblioteca.entity.Jogo;
import br.com.biblioteca.exception.BibliotecaException;
import io.swagger.annotations.ApiParam;

@Path("jogo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class JogoService {
	
	@Inject
	private JogoDAO dao;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvar(@ApiParam(value="jogo", required=true) final Jogo jogo){
		if(jogo == null) 
			throw new BibliotecaException("Dados n�o informados");
		dao.salvar(jogo);		
		return Response.ok(jogo).build();
	}
	
	@PUT
	public Response atualizar(@ApiParam(value="autor", required=true) final Jogo jogo){
		if(jogo == null) 
			throw new BibliotecaException("Dados n�o informados");
		dao.alterar(jogo);		
		return Response.ok(jogo).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response excluir(@PathParam("id") final Integer id){
		if(id == null) 
			throw new BibliotecaException("Dados n�o informados");		
		Jogo jogo = dao.buscarJogoPorId(id);		
		dao.excluir(jogo);		
		return Response.ok("Excluido com sucesso!").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response obterTodosOsAutores(){
		List<Jogo> jogos = dao.buscarTodosOsJogos();		
		return Response.ok(jogos).build();
	}

}
