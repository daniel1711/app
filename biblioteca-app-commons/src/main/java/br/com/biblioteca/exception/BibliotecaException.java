package br.com.biblioteca.exception;

public class BibliotecaException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BibliotecaException(String message) {
		super(message);
	}

}
