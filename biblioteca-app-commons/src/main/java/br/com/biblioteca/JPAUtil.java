package br.com.biblioteca;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	public static EntityManager getCreateEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("console");
		EntityManager manager = factory.createEntityManager();
		return manager;
	}

}
